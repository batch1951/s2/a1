package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//A repository contains methods for database manipulation, which it has inherited from the CrudRepository's pre-defined methods.
@Repository
public interface UserRepository extends CrudRepository<User, Object> {


}
